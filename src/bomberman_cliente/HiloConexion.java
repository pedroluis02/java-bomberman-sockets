
package bomberman_cliente;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import bomberman_servidor.Mensaje;

public class HiloConexion extends Thread {
    private Socket socket;
    private ClienteGUI clienteGUI;
    private PanelJuego panelJuego;
    private DataInputStream lectura;
    private DataOutputStream escritura;
    //
    public String id;
    public String nombre;
    public int numero;
    public String idOponente;
    
    public HiloConexion(Socket socket, ClienteGUI clienteGUI, PanelJuego panelJuego, String nombre) {
        this.socket = socket;
        this.clienteGUI = clienteGUI;
        this.panelJuego = panelJuego;
        this.numero = -1;
        this.idOponente = "";
        this.id = "";
        this.nombre = nombre;
        try {
            lectura = new DataInputStream(socket.getInputStream());
            escritura = new DataOutputStream(socket.getOutputStream());
        } catch(Exception ex) {
            clienteGUI.mostrarMensaje(ex.getMessage());
        }
    }   
    @Override
    public void run() {
        int tipo;
        try {
            while(true) {
                tipo = lectura.readInt();
                switch(tipo) {
                    case Mensaje.ID:
                        id = lectura.readUTF();
                        clienteGUI.mostrarMensaje("Registro: id=" + id);
                        enviarDatos(nombre);
                        break;
                    case Mensaje.INICIO: 
                        numero = lectura.readInt();
                        idOponente = lectura.readUTF(); 
                        panelJuego.incioJuego(numero);
                        clienteGUI.mostrarMensaje("Inicio: Oponente=" + idOponente);
                        break;
                    case Mensaje.DESCONECTADO:
                        String m = lectura.readUTF();
                        clienteGUI.mostrarMensaje(m);
                        clienteGUI.cambioGUIDesconectado();
                        clienteGUI.mostrarMensaje_D(m);
                        break;
                    case Mensaje.MOVIMIENTO: int io = lectura.readInt(),
                                jo = lectura.readInt();
                        panelJuego.movimientoOponente(io, jo);
                        clienteGUI.mostrarMensaje("Mov Oponente: M(" + io + ", " + jo + ")");
                        break;
                    case Mensaje.BOMBA: 
                        int ib = lectura.readInt(),
                            jb = lectura.readInt();
                        panelJuego.colocarBombaOponente(ib, jb);
                        break;
                }
            }
        } catch(Exception ex) {
            clienteGUI.mostrarMensaje(ex.getMessage());
        }
        
        clienteGUI.mostrarMensaje("Conexión con servidor perdida: juego finalizado");
        clienteGUI.cambioGUIDesconectado();
        clienteGUI.mostrarMensaje_D("Conexión con servidor perdida");
    }

    // mensajes
    public void enviarDatos(String nombre) {
        try {
            escritura.writeInt(Mensaje.DATOS);
            escritura.writeUTF(nombre);
        } catch(Exception ex) {
            clienteGUI.mostrarMensaje(ex.getMessage());
        }
    }
    public void enviarMovimiento(int i, int j) {
        try {
            escritura.writeInt(Mensaje.MOVIMIENTO);
            escritura.writeInt(i);
            escritura.writeInt(j);
        } catch(Exception ex) {
            clienteGUI.mostrarMensaje(ex.getMessage());
        }
    }
    public void enviarBomba(int i, int j) {
        try {
            escritura.writeInt(Mensaje.BOMBA);
            escritura.writeInt(i);
            escritura.writeInt(j);
        } catch(Exception ex) {
            clienteGUI.mostrarMensaje(ex.getMessage());
        }
    }
}
