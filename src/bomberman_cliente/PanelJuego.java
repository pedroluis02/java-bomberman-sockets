
package bomberman_cliente;

import jugador.Bomba;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.TexturePaint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.Timer;
import jugador.Jugador;

public class PanelJuego extends JPanel implements  ActionListener {
    //
    public int matrizJuego[][];
    private int numero_filas;
    private int numero_columnas;
    //
    private ClienteGUI clienteGUI;
    // cuadros
    public Rectangle2D[][] cuadros;
    private int anchoCuadro;
    private int altoCuadro;
   
    private int RX, RY;
    
    public boolean iniciado, fin, fin_OP;
    //
    private Jugador jugador, jugador_OP;
    //
    private Bomba bomba, bomba_OP;
    //
    private Timer tiempoBomba, tiempoExplosion;
    private Timer tiempoBomba_OP, tiempoExplosion_OP;
    //
    private Timer tiempoFin, tiempoFin_OP;
    //
    BufferedImage BLOQUE_I, FONDOI;
    //
    private boolean ganador;

    public PanelJuego(ClienteGUI parent, int width, int height) {
        try {
            setPreferredSize(new Dimension(width, height));
            this.clienteGUI = parent;
            this.iniciado = false;
            this.fin = false; 
            fin_OP = false;
            ganador = false;
            
            numero_filas = 11;
            numero_columnas = 11;
            
            matrizJuego = new int[numero_filas][numero_columnas];
            anchoCuadro  = width / numero_filas;
            altoCuadro = height / numero_columnas;
            
            cuadros = new Rectangle2D[numero_filas][numero_columnas]; 
            
            iniciarMatriz_Cuadros();
            setBackground(Color.WHITE);
            
            //
            RX = anchoCuadro / 3; RY = altoCuadro / 3;
            
            jugador = new Jugador(this);
            jugador_OP = new Jugador(this);
            
            bomba = new Bomba();
            bomba_OP = new Bomba();
            
            tiempoBomba = new Timer(600, this); tiempoExplosion = new Timer(25, this);
            tiempoBomba_OP = new Timer(600, this); tiempoExplosion_OP = new Timer(25, this);
            
            tiempoBomba.setActionCommand("b1"); tiempoExplosion.setActionCommand("e1");
            tiempoBomba_OP.setActionCommand("b2"); tiempoExplosion_OP.setActionCommand("e2");
            
            tiempoFin = new Timer(10, this); tiempoFin.setActionCommand("tf");
            tiempoFin_OP = new Timer(10, this); tiempoFin_OP.setActionCommand("tfo");
            
            BLOQUE_I = ImageIO.read(
                    new File(getClass().getResource("/recursos/bloque.png").getPath()));
            
            FONDOI = ImageIO.read(
                    new File(getClass().getResource("/recursos/fondo.png").getPath()));
            
            // prueba
//            iniciado = true;
//            jugador.numero = 0;
//            jugador.setPosicion(1, 1);
//            jugador.setColorFondoAvatar(Color.RED);
        } catch (IOException ex) {
        }
    }
    public void incioJuego(int numero) {
        if(numero == 0) {
            jugador.setNumero(numero);
            jugador.setPosicion(1, 1);
            jugador.setColorFondoAvatar(Color.RED);
            //
            jugador_OP.setPosicion(9, 9);
            jugador_OP.setColorFondoAvatar(Color.BLUE);
        } else {
            jugador.setNumero(numero);
            jugador.setPosicion(9, 9);
            jugador.setColorFondoAvatar(Color.BLUE);
            //
            jugador_OP.setPosicion(1, 1);
            jugador_OP.setColorFondoAvatar(Color.RED);
        }
        iniciado = true;
        repaint();
    }
    private void iniciarMatriz_Cuadros() {
       for(int i = 0; i < numero_filas; i++) {
            for (int j = 0; j < numero_columnas; j++) {
                if(i == 0 || i==(numero_filas-1) || j==0 || j==(numero_columnas-1)) {
                    matrizJuego[i][j] = 0;
                } else if(i%2==0 && j%2==0)  {
                    matrizJuego[i][j] = 0;
                }
                else {
                    matrizJuego[i][j] = 1;
                }
                cuadros[i][j] = new Rectangle2D.Double(i*anchoCuadro, j*altoCuadro,
                        anchoCuadro, altoCuadro);
            }
        } 
    }
    
    public void iniciarCuadros() {  
        for(int i = 0; i < numero_filas; i++) {
            for (int j = 0; j < numero_columnas; j++) {
                cuadros[i][j] = new Rectangle2D.Double(i*anchoCuadro, j*altoCuadro,
                        anchoCuadro, altoCuadro);
            }
        }
    }
 
    private void drawCuadros(Graphics2D g) {
        /*
         * Para crear un gradiente vertical,
         * las coordenadas a usar seran del {0, 0} al {0, altura del componente}
         * GradientPaint verticalGradient;
         * verticalGradient = new GradientPaint(
                d.x, d.y, Color.GRAY, 
                d.x , d.y + d.height, Color.BLACK); 
         */
        
        for(int i = 0; i<numero_filas; i++) {
            for(int j = 0; j < numero_columnas; j++) {
                TexturePaint tp;
                if(matrizJuego[i][j] == 0) {
                    tp = new TexturePaint(BLOQUE_I, cuadros[i][j]);
                }
                else {
                    tp = new TexturePaint(FONDOI, cuadros[i][j]); 
                }
                g.setPaint(tp);
                g.fill(cuadros[i][j]);
            }
        }
    }
    
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        drawCuadros(g2);
        
        if(iniciado) {
            if(!fin) {
                jugador.drawJugador(g2, RX, RY);
            } 
            if(!fin_OP) {
                jugador_OP.drawJugador(g2, RX, RY);
            } 
            
            if(bomba.colocada) {
                bomba.drawBomba(g2, this);
            }
            if(bomba_OP.colocada) {
                bomba_OP.drawBomba(g2, this);
            }
            
            if(bomba.explosion) {
                bomba.drawExplosion(g2);
            }
            
            if(bomba_OP.explosion) {
                bomba_OP.drawExplosion(g2);
            }
        }
    }
    /// ---
    
    public Point ejecutarMovimiento(int tipo) {
        Point p = jugador.ejecutarMovimiento(tipo);
        repaint();
        return p;
    }
    public void movimientoOponente(int posI, int posJ) {
        jugador_OP.setPosicion(posI, posJ);
        repaint();
    }
    //
    public void colocarBomba() {
       bomba.setPosicion(jugador.posI, jugador.posJ, cuadros, matrizJuego);
       bomba.colocada = true;
       repaint();
       tiempoBomba.start();
    }
    public void colocarBombaOponente(int posI, int posJ) {
       bomba_OP.setPosicion(posI, posJ, cuadros, matrizJuego);
       bomba_OP.colocada = true;
       repaint();
       tiempoBomba_OP.start();
    }
    
    @Override
    public void actionPerformed(ActionEvent ae){
        String a = ae.getActionCommand();
        if(a.compareTo("b1") == 0 ) {
            tiempoBomba.stop();
            tiempoExplosion.start();
            bomba.explosion = true;
            bomba.colocada = false;
            repaint();
        } else if(a.compareTo("b2") == 0 ) {
            tiempoBomba_OP.stop();
            tiempoExplosion_OP.start();
            bomba_OP.explosion = true;
            bomba_OP.colocada = false;
            repaint();
        } else if(a.compareTo("e1") == 0 ) {
            //
            analisisExplosionBomba();
            //
            bomba.aumentoExplosion();
            repaint();
            if(bomba.cantidad == Bomba.MAXIMO) {
                tiempoExplosion.stop();
                bomba.explosion = false;
                repaint();
                
                if(fin_OP && fin==false) {
                    clienteGUI.mostrarMensaje_D("Ganador");
                    fin = false; fin_OP = false;
                    incioJuego(jugador.numero);
                    ganador = true;
                }
            }
        } else if(a.compareTo("e2") == 0 ) {
            //
            analisisExplosionBombaOponente();
            //
            bomba_OP.aumentoExplosion();
            repaint();
            if(bomba_OP.cantidad == Bomba.MAXIMO) {
                tiempoExplosion_OP.stop();
                bomba_OP.explosion = false;
                repaint();
                
                if(fin_OP && fin==false) {
                    clienteGUI.mostrarMensaje_D("Ganador");
                    fin = false; fin_OP = false;
                    incioJuego(jugador.numero);
                    ganador = true;
                }
            }
        } else if(a.compareTo("tf") == 0 ) {
            tiempoFin.stop();
            clienteGUI.mostrarMensaje_D("Has Muerto");
            incioJuego(jugador.numero);
            fin = false; fin_OP = false;
        } else if(a.compareTo("tfo") == 0 ) {
            tiempoFin_OP.stop();
            if(fin == false && !bomba.explosion && !bomba_OP.explosion) {
                clienteGUI.mostrarMensaje_D("Ganador");
                fin = false; fin_OP = false;
                incioJuego(jugador.numero);
            }
        }
    }
    //
    private void analisisExplosionBomba() {
        if(fin == false) {
            if(bomba.posI == jugador.posI && bomba.posJ == jugador.posJ) {
                tiempoFin.start();
                fin = true;
            } else if(bomba.posI - 1 == jugador.posI && bomba.posJ == jugador.posJ &&
                    bomba.cantidad >= 6) {
                tiempoFin.start();
                fin = true;
            }  else if(bomba.posI + 1 == jugador.posI && bomba.posJ == jugador.posJ
                    &&  bomba.cantidad >= 6) {
                tiempoFin.start();
                fin = true;
            }  else if(bomba.posI == jugador.posI && bomba.posJ - 1 == jugador.posJ
                    &&  bomba.cantidad >= 6) {
                tiempoFin.start();
                fin = true;
            }
             else if(bomba.posI == jugador.posI && bomba.posJ + 1 == jugador.posJ
                     &&  bomba.cantidad >= 6) {
                tiempoFin.start();
                fin = true;
            }
        }

        //
        if(fin_OP == false) {
            if(bomba.posI == jugador_OP.posI && bomba.posJ == jugador_OP.posJ) {
                tiempoFin_OP.start();
                fin_OP = true;
            } else if(bomba.posI - 1 == jugador_OP.posI && bomba.posJ == jugador_OP.posJ &&
                    bomba.cantidad >= 6) {
                tiempoFin_OP.start();
                fin_OP = true;
            }  else if(bomba.posI + 1 == jugador_OP.posI && bomba.posJ == jugador_OP.posJ
                    &&  bomba.cantidad >= 6) {
                tiempoFin_OP.start();
                fin_OP = true;
            }  else if(bomba.posI == jugador_OP.posI && bomba_OP.posJ - 1 == jugador_OP.posJ
                    &&  bomba.cantidad >= 6) {
                tiempoFin_OP.start();
                fin_OP = true;
            }
             else if(bomba.posI == jugador_OP.posI && bomba.posJ + 1 == jugador_OP.posJ
                     &&  bomba.cantidad >= 6) {
                tiempoFin_OP.start();
                fin_OP = true;
            }
        }
    }
    
    private void analisisExplosionBombaOponente() {
        if(fin == false) {
            if(bomba_OP.posI == jugador.posI && bomba_OP.posJ == jugador.posJ) {
                tiempoFin.start();
                fin = true;
            } else if(bomba_OP.posI - 1 == jugador.posI && bomba_OP.posJ == jugador.posJ &&
                    bomba_OP.cantidad >= 6) {
                tiempoFin.start();
                fin = true;
            }  else if(bomba_OP.posI + 1 == jugador.posI && bomba_OP.posJ == jugador.posJ
                    &&  bomba_OP.cantidad >= 6) {
                tiempoFin.start();
                fin = true;
            }  else if(bomba_OP.posI == jugador.posI && bomba_OP.posJ - 1 == jugador.posJ
                    &&  bomba_OP.cantidad >= 6) {
                tiempoFin.start();
                fin = true;
            }
             else if(bomba_OP.posI == jugador.posI && bomba_OP.posJ + 1 == jugador.posJ
                     &&  bomba_OP.cantidad >= 6) {
                tiempoFin.start();
                fin = true;
            }
        }
        
        //
        if(fin_OP == false) {
            if(bomba_OP.posI == jugador_OP.posI && bomba_OP.posJ == jugador_OP.posJ) {
                tiempoFin_OP.start();
                fin_OP = true;
            } else if(bomba_OP.posI - 1 == jugador_OP.posI && bomba_OP.posJ == jugador_OP.posJ &&
                    bomba_OP.cantidad >= 6) {
                tiempoFin_OP.start();
                fin_OP = true;
            }  else if(bomba_OP.posI + 1 == jugador_OP.posI && bomba_OP.posJ == jugador_OP.posJ
                    &&  bomba_OP.cantidad >= 6) {
                tiempoFin_OP.start();
                fin_OP = true;
            }  else if(bomba_OP.posI == jugador_OP.posI && bomba_OP.posJ - 1 == jugador_OP.posJ
                    &&  bomba_OP.cantidad >= 6) {
                tiempoFin_OP.start();
                fin_OP = true;
            }
             else if(bomba_OP.posI == jugador_OP.posI && bomba_OP.posJ + 1 == jugador_OP.posJ
                     &&  bomba_OP.cantidad >= 6) {
                tiempoFin_OP.start();
                fin_OP = true;
            }
        }
    }
    //
    public boolean isBombaActiva() {
        return bomba.colocada || bomba.explosion;
    }
    //
    public Point getPosicion() {
        return new Point(jugador.posI, jugador.posJ);
    }
}
