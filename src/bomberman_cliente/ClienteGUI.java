
package bomberman_cliente;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.net.InetAddress;
import java.net.Socket;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;
import jugador.Jugador;

public class ClienteGUI extends JFrame implements ActionListener, KeyListener {

    public ClienteGUI() {
 
        initComponents();
        panelJuego = new PanelJuego(this, 594, 594);
        JPanel p = new JPanel();
        p.add(panelJuego);
        jScrollPane2.setViewportView(p);
        pack();
        
        panelJuego.addKeyListener(this);
        
        tiempoMensaje = new Timer(5000,  this); 
        tiempoMensaje.setActionCommand("tiempo");
        //
        try {
            nombre = InetAddress.getLocalHost().getHostName();
            host = InetAddress.getLocalHost().getHostAddress();
        } catch(Exception ex) {
            host = "localhost";
        }
        //
        panelJuego.requestFocus();
        
        textoNombre.setText(nombre);
        textoIP.setText(host);
        textoPuerto.setText(puerto + "");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        textoIP = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        textoPuerto = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        botonConectar = new javax.swing.JButton();
        botonDesconectar = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        areaMensajes = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();
        textoNombre = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos"));

        jLabel1.setLabelFor(textoIP);
        jLabel1.setText("Dirección IP");

        jLabel2.setLabelFor(textoPuerto);
        jLabel2.setText("Puerto");

        jPanel3.setLayout(new java.awt.GridLayout(2, 1, 0, 5));

        botonConectar.setText("Conectar");
        botonConectar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonConectarActionPerformed(evt);
            }
        });
        jPanel3.add(botonConectar);

        botonDesconectar.setText("Desconectar");
        botonDesconectar.setEnabled(false);
        botonDesconectar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonDesconectarActionPerformed(evt);
            }
        });
        jPanel3.add(botonDesconectar);

        jPanel4.setLayout(new java.awt.BorderLayout());

        areaMensajes.setEditable(false);
        areaMensajes.setColumns(20);
        areaMensajes.setRows(5);
        jScrollPane1.setViewportView(areaMensajes);

        jPanel4.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jLabel3.setText("Nombre");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textoNombre)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(textoIP)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 175, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(textoPuerto)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textoNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textoIP, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textoPuerto, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(44, Short.MAX_VALUE))
        );

        jPanel2.setLayout(new javax.swing.BoxLayout(jPanel2, javax.swing.BoxLayout.PAGE_AXIS));

        jScrollPane2.setBorder(javax.swing.BorderFactory.createTitledBorder("Juego"));
        jPanel2.add(jScrollPane2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 636, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonConectarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonConectarActionPerformed
        // TODO add your handling code here:
        host = textoIP.getText();
        try {
        puerto = Integer.parseInt(textoPuerto.getText());
        } catch(Exception ex) {
            mostrarMensaje("Error en puerto");
            return;
        }
        conectar(host, puerto);
    }//GEN-LAST:event_botonConectarActionPerformed

    private void botonDesconectarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonDesconectarActionPerformed
        // TODO add your handling code here:
        desconectar();
    }//GEN-LAST:event_botonDesconectarActionPerformed

    public void conectar(String ip, int puerto) {
        try {
            socket = new Socket(ip, puerto);
            hiloServidor = new HiloConexion(socket, this, panelJuego, nombre);
            hiloServidor.start();

            cambioGUIConectado();
            
        } catch(Exception ex) {
            mostrarMensaje(ex.getMessage());
        }
    }
    public void desconectar() {
        try {
            hiloServidor.stop();
            socket.close();
            
            cambioGUIDesconectado();
            
        } catch(Exception ex) {
            mostrarMensaje(ex.getMessage());
        }
    }
    //
    public void cambioGUIConectado() {
        botonConectar.setEnabled(false);
        botonDesconectar.setEnabled(true);
        //
        textoNombre.setEditable(false);
        textoIP.setEditable(false);
        textoPuerto.setEditable(false);
        //
        panelJuego.requestFocus();
        //
    }
    public void cambioGUIDesconectado() {
        botonConectar.setEnabled(true);
        botonDesconectar.setEnabled(false);
        //
        textoNombre.setEditable(true);
        textoIP.setEditable(true);
        textoPuerto.setEditable(true);
        //
        areaMensajes.setText("");
        panelJuego.iniciado = false;
        panelJuego.fin = false;
        panelJuego.fin_OP = false;
    }
    //
    public void mostrarMensaje(String mensaje) {
        areaMensajes.append(mensaje + "\n");
        System.out.println(mensaje);
    }
    public void mostrarMensaje_D(String mensaje) {
        JOptionPane.showMessageDialog(this, mensaje);
    }
    //
    @Override
    public void actionPerformed(ActionEvent ae) {
    }
    @Override
    public void keyPressed(KeyEvent ke) {
        if(!panelJuego.iniciado || panelJuego.fin) {
            return;
        }
        Point p = null;
        switch(ke.getKeyCode()) {
            case KeyEvent.VK_A: p = panelJuego.ejecutarMovimiento(Jugador.MOV_IZQUIERDA);
                break;
            case KeyEvent.VK_D: p = panelJuego.ejecutarMovimiento(Jugador.MOV_DERECHA);
                break;
            case KeyEvent.VK_W: p = panelJuego.ejecutarMovimiento(Jugador.MOV_ARRIBA);
                break;
            case KeyEvent.VK_S: p = panelJuego.ejecutarMovimiento(Jugador.MOV_ABAJO);
                break;
            case KeyEvent.VK_CONTROL:
                if(panelJuego.isBombaActiva())  {
                    break;
                }
                Point d = panelJuego.getPosicion();
                hiloServidor.enviarBomba(d.x, d.y);
                panelJuego.colocarBomba();
                break;
        }
        if(p != null) {
            hiloServidor.enviarMovimiento(p.x, p.y);
        }
    }
    @Override
    public void keyTyped(KeyEvent ke) { 
    }
    @Override
    public void keyReleased(KeyEvent ke) { 
    }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ClienteGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ClienteGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ClienteGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ClienteGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        ClienteGUI b = new ClienteGUI();
        b.setTitle("Juego Cliente");
        b.setLocationRelativeTo(null);
        b.setVisible(true);
        //b.conectar("localhost", 5000);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea areaMensajes;
    private javax.swing.JButton botonConectar;
    private javax.swing.JButton botonDesconectar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField textoIP;
    private javax.swing.JTextField textoNombre;
    private javax.swing.JTextField textoPuerto;
    // End of variables declaration//GEN-END:variables

    private Timer tiempoMensaje;
    private PanelJuego panelJuego;
    private HiloConexion hiloServidor;
    private Socket socket;
    
    //
    private String host;
    private String nombre;
    private int puerto = 5000;
}
