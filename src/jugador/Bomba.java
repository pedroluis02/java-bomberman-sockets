
package jugador;

import bomberman_cliente.PanelJuego;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import javax.swing.ImageIcon;

public class Bomba {
    //
    public static final int MINIMO = 2;
    public static final int MAXIMO = 13; // 13 // 6
    //
    public int posI, posJ;
    //
    private double izqX, derX;
    private double arrY, abaY;
    //
    private double aumentoX, aumentoY;
    private double RX, RY;
    public int cantidad;
    //
    private int V_izqX, V_derX;
    private int V_arrY, V_abaY;
    //
    private int cont_izqX, cont_derX;
    private int cont_arrY, cont_abaY;
    //
    public boolean colocada = false;
    public boolean explosion = false;
    //
    private double centroX, centroY;
    //
    private ImageIcon imagenBomba;
    
    public Bomba() {
        colocada = false;
        explosion = false;
    }
    public void setPosicion(int posI, int posJ, 
            Rectangle2D[][] cuadros, int[][] matrizJuego) {
        this.posI = posI; this.posJ = posJ;
        cantidad = 0;
        
        Rectangle2D r2 = cuadros[posI][posJ];
        centroX = r2.getCenterX();
        centroY = r2.getCenterY();
        //
        RX = r2.getWidth() / 2;
        RY = r2.getHeight() / 2;
        //
        aumentoX = 5 
                //r2.getWidth()/8
                ;
        aumentoY = 5
                //r2.getWidth()/8
                ;
        //
        izqX = centroX;  derX = centroX;
        
        abaY = centroY;  arrY = centroY;
        
        //
        V_izqX = matrizJuego[posI - 1][posJ] == 0 ? MINIMO : MAXIMO;        
        V_derX = matrizJuego[posI + 1][posJ] == 0 ? MINIMO : MAXIMO; 
        
        V_abaY = matrizJuego[posI][posJ + 1] == 0 ? MINIMO : MAXIMO;        
        V_arrY = matrizJuego[posI][posJ - 1] == 0 ? MINIMO : MAXIMO; 
        
        cont_izqX = 0; cont_derX = 0;
        cont_arrY = 0; cont_abaY = 0;
        
        imagenBomba = new ImageIcon(getClass().getResource("/recursos/bomba.png"));
    }
    public void drawBomba(Graphics2D g, PanelJuego pj) {
        int x = (int)centroX - imagenBomba.getIconWidth()/2;
        int y = (int)centroY - imagenBomba.getIconHeight()/2;
        
        g.drawImage(imagenBomba.getImage(), x, y, pj);
    }
    public void drawExplosion(Graphics2D g) {
        g.setColor(new Color(255, 149, 14));
        g.fillOval((int)(centroX - RX), (int)(centroY - RY), 
                (int)(RX + RX), (int)(RY + RY));
        //
        g.setStroke(new BasicStroke(32.0f));
        g.drawLine((int)centroX, (int)centroY, (int)izqX, (int)centroY);
        g.drawLine((int)centroX, (int)centroY, (int)centroX, (int)abaY);
        g.drawLine((int)centroX, (int)centroY, (int)derX, (int)centroY);
        g.drawLine((int)centroX, (int)centroY, (int)centroX, (int)arrY);
        //
        g.setStroke(new BasicStroke(1.0f));
    }
    public void aumentoExplosion() {
        if(cont_izqX < V_izqX) {
           izqX -= aumentoX; 
           cont_izqX++;
        } 
        
        if(cont_derX < V_derX) {
            derX += aumentoX;
            cont_derX++;
        }
        
        if(cont_abaY < V_abaY) {
            abaY += aumentoY;
           cont_abaY++; 
        }
        
        if(cont_arrY < V_arrY) {
           arrY -= aumentoY; 
           cont_arrY++;
        }
        
        cantidad++;
    }
}
