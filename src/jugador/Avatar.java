
package jugador;

import bomberman_cliente.PanelJuego;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import javax.swing.ImageIcon;

public class Avatar {
    // Iconos de estado de avatar
    public ImageIcon ICONO_ABA = imagenAvatarRecurso("mov_abajo.gif");
    public ImageIcon ICONO_IZQ = imagenAvatarRecurso("mov_izquierda.gif");
    public ImageIcon ICONO_DER = imagenAvatarRecurso("mov_derecha.gif");
    public ImageIcon ICONO_NINGUNO = imagenAvatarRecurso("mov_ninguno.gif");
    //
    private int posX, posY;
    public ImageIcon imagenAvatar; // Imagen de avatar inicial
    private Ellipse2D fondoAvatar;
    private Color colorFondo;
    //
    public Rectangle2D [][]cuadros;
    
    public Avatar() {
        colorFondo = Color.BLACK;
        imagenAvatar = ICONO_NINGUNO;
        fondoAvatar = new Ellipse2D.Double() ;
        posX = 0; posY = 0;
    }

    public void setColorFondo(Color colorFondo) {
        this.colorFondo = colorFondo;
    }
    
    private void calcularPosFondoAvatar(Rectangle r, int rX, int rY) {
        int x = (int)r.getCenterX() - rX,
            y = (int)r.getCenterY() - rY;
        fondoAvatar.setFrame(x, y, rX + rX, rY + rY);
    }
    
    private void calcularPosImagenAvatar() {
        posX = (int)fondoAvatar.getCenterX() 
                - imagenAvatar.getIconWidth()/2;
        posY = (int)fondoAvatar.getBounds().getCenterY() 
                - imagenAvatar.getIconHeight()/2;
    }
    
    public void drawAvatar(Graphics2D g, PanelJuego pj, int posI, int posJ, int rX, int rY) {
        calcularPosFondoAvatar(pj.cuadros[posI][posJ].getBounds(), rX, rY);
        g.setColor(colorFondo);
        g.fill(fondoAvatar);
        // imagen avatar
        calcularPosImagenAvatar();
        g.drawImage(imagenAvatar.getImage(), posX, posY, pj);
    }
    
    //
    public static ImageIcon imagenAvatarRecurso(String nombre) {
        return new ImageIcon(
                PanelJuego.class.getResource("/recursos/" + nombre));
    }
}
