
package jugador;

import bomberman_cliente.PanelJuego;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

public class Jugador {
    //
    public static final int MOV_IZQUIERDA = 11;
    public static final int MOV_DERECHA = 22;
    public static final int MOV_ARRIBA = 33;
    public static final int MOV_ABAJO = 44;
    //
    //
    public int numero;
    private Avatar avatar;
    public int posI, posJ;
    
    public int numeroMovIJ;
    
    private PanelJuego panelJuego;
    
    public Jugador(PanelJuego pj) {
        numero = -1;
        panelJuego = pj;
        numeroMovIJ = 0;
        avatar = new Avatar();
    }
    
    private boolean isMovimientoValido_J(int j) {
        return panelJuego.matrizJuego[posI][j] == 1;
    }
    private boolean isMovimientoValido_I(int i) {
        return panelJuego.matrizJuego[i][posJ] == 1;
    }
    
    // set
    public void setPosicion(int posI, int posJ) {
        this.posI = posI; this.posJ = posJ;
    }
    public void setNumero(int numero) {
        this.numero = numero;
    }
    public void setColorFondoAvatar(Color colorFondo) {
        avatar.setColorFondo(colorFondo);
    }
    
    public Point ejecutarMovimiento(int tipo) {        
        int v;
        switch(tipo) {
            case MOV_IZQUIERDA: v = posI; 
                v--;
                if(isMovimientoValido_I(v)) {
                    posI = v;
                }
                break;
            case MOV_DERECHA: v = posI;
                v++;
                if(isMovimientoValido_I(v)) {
                    posI = v;
                }
                break;
            case MOV_ARRIBA: v = posJ;
                v--;
                if(isMovimientoValido_J(v)) {
                    posJ = v;
                }
                break;
            case MOV_ABAJO: v = posJ;
                v++;
                if(isMovimientoValido_J(v)) {
                    posJ = v;
                }
                break;
        }
        return new Point(posI, posJ);
    }
    //
    public void drawJugador(Graphics2D g, int rX, int rY) {
        avatar.drawAvatar(g, panelJuego, posI, posJ, rX, rY);
    }
}
