
package bomberman_servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class HiloCliente extends Thread {
    public Socket socket;
    private ServidorGUI servidorGUI;
    private DataInputStream lectura;
    private DataOutputStream escritura;
    //
    public String id;
    public String nombre;
    public int numero;
    public String idOponente;
    
    public HiloCliente(Socket socket, ServidorGUI servidorGUI, int numero) {
        this.socket = socket;
        this.servidorGUI = servidorGUI;
        this.numero = numero;
        this.idOponente = "";
        try {
            lectura = new DataInputStream(socket.getInputStream());
            escritura = new DataOutputStream(socket.getOutputStream());
        } catch(IOException ex) {
            servidorGUI. mostarMensaje(ex.getMessage());
        }        
    }
    @Override
    public void run() {
        try {
            int tipo;
            while(true) {
               tipo = lectura.readInt();
               switch(tipo) {
                   case Mensaje.DATOS:
                       nombre = lectura.readUTF();
                       servidorGUI.mostarMensaje("Datos: id=" + id + " nombre=" + nombre);
                       break;
                   case Mensaje.MOVIMIENTO:
                       int io = lectura.readInt(),
                           jo = lectura.readInt();
                       ServidorGUI.jugadores.get(idOponente).enviarMovimiento(io, jo);
                       servidorGUI.mostarMensaje("Mov: id=" + id +
                               " [i=" + io + " j=" + jo + "] para=" + idOponente);
                       break;
                   case Mensaje.BOMBA:
                       int ib = lectura.readInt(),
                           jb = lectura.readInt();
                       ServidorGUI.jugadores.get(idOponente).enviarBomba(ib, jb);
                       servidorGUI.mostarMensaje("Bomba id=" + id +
                               " i=" + ib + " j=" + jb + " para=" + idOponente);
                       break;
               } 
            }
        } catch(Exception ex) {
            servidorGUI. mostarMensaje(ex.getMessage());
        }

        servidorGUI.desconectadoJugador(id);
    }
    // mensajes
    public void enviarIdentificador(String id) {
        this.id = id;
        try {
            escritura.writeInt(Mensaje.ID);
            escritura.writeUTF(id);
            servidorGUI.mostarMensaje("Registro: id=" + id);
        } catch(Exception ex) {
            servidorGUI.mostarMensaje(ex.getMessage());
        }
    } 
    public void enviarInicio(int num, String idOp) {
        idOponente = idOp;
        numero = num;
        try {
            escritura.writeInt(Mensaje.INICIO);
            escritura.writeInt(numero);
            //
            escritura.writeUTF(idOp);
            servidorGUI.mostarMensaje("Inicio: J1=" + id + " j2=" + idOp);
        } catch(Exception ex) {
            servidorGUI.mostarMensaje(ex.getMessage());
        }
    }
    public void enviarMovimiento(int i, int j) {
        try {
            escritura.writeInt(Mensaje.MOVIMIENTO);
            escritura.writeInt(i);
            escritura.writeInt(j);
        } catch(Exception ex) {
            servidorGUI.mostarMensaje(ex.getMessage());
        }
    }
    public void enviarBomba(int i, int j) {
        try {
            escritura.writeInt(Mensaje.BOMBA);
            escritura.writeInt(i);
            escritura.writeInt(j);
        } catch(Exception ex) {
            servidorGUI.mostarMensaje(ex.getMessage());
        }
    }
    public void enviarDesconectado(String mensaje) {
        try {
            escritura.writeInt(Mensaje.DESCONECTADO);
            escritura.writeUTF(mensaje);
        } catch(Exception ex) {
            servidorGUI.mostarMensaje(ex.getMessage());
        }
    }
}
