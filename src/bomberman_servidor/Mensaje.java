
package bomberman_servidor;

public class Mensaje {
    public static final int ID = 0;
    public static final int INICIO = 1;
    public static final int MOVIMIENTO = 2;
    public static final int BOMBA = 3;
    public static final int DATOS = 4;
    public static final int DESCONECTADO = 5;
}
