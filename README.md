# Multiplayer Bomberman: Player 1 vs Player 2 
* A simple game inspired on classic Bomberman game. 
* It is  based on Java sockets.
* It contains an implementation of a server and a client socket.

Keyboard control
----------------
key | Description
| --- | --- |
A | Left
w | Up
D | Right
S | Down
Ctrl | Put a bomb
